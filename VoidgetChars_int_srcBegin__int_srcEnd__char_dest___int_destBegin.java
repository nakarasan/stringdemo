package stringdemo;

public class VoidgetChars_int_srcBegin__int_srcEnd__char_dest___int_destBegin {

	public static void main(String[] args) {

		String Str1 = new String("Welcome to Kara's Java Wolrd");
		char[] Str2 = new char[7];
		try {
			Str1.getChars(10, 15, Str2, 0);
			System.out.print("Copied Value = ");
			System.out.println(Str2);
		} catch (Exception ex) {
			System.out.println("Raised exception...");
		}

	}

}
