package stringdemo;

public class BooleanEqualsIgnoreCase_String_String {

	public static void main(String[] args) {
		String str1 = "BCAS Jaffna Campus";
		String str2 = "Jaffna";
		String str3 = "bcasJaffna Campus";

		boolean equals1 = str1.equalsIgnoreCase(str2);
		boolean equals2 = str1.equalsIgnoreCase(str3);
		System.out.println();

		System.out.println("\"" + str1 + "\" equals \"" + str2 + "\"? " + equals1);
		System.out.println("\"" + str1 + "\" equals \"" + str3 + "\"? " + equals2);

	}

}
