package stringdemo;

public class IntCompateTo_String_String {

	public static void main(String[] args) {
		String str01 = "Strings are immutable";
		String str02 = "Strings are immutable";
		String str03 = "Integers are not immutable";

		int result = str01.compareTo(str02);
		System.out.println(result);

		result = str02.compareTo(str03);
		System.out.println(result);

		result = str03.compareTo(str01);
		System.out.println(result);

	}

}
