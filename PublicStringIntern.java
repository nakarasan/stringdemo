package stringdemo;

public class PublicStringIntern {

	public static void main(String[] args) {

		String Str1 = new String("Welcome to Kara's Java World");
		String Str2 = new String("WELCOME TO MY WORLD");

		System.out.print("Canonical representation : ");
		System.out.println(Str1.intern());

		System.out.print("Canonical representation : ");
		System.out.println(Str2.intern());

	}

}
