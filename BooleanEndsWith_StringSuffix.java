package stringdemo;

public class BooleanEndsWith_StringSuffix {

	public static void main(String[] args) {
		String StrX = new String("This is really not immutable!!");
		boolean retVal;

		retVal = StrX.endsWith("immutable!!");
		System.out.println("Returned Value = " + retVal);

		retVal = StrX.endsWith("immu");
		System.out.println("Returned Value = " + retVal);

	}

}
