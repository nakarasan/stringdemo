package stringdemo;

public class BooleanEquals_Object_obj {
	public static void main(String[] args) {
		Boolean b1 = new Boolean(true);
		Object obj1 = new Boolean(false);

		if (b1.equals(obj1)) {
			System.out.println("They are equal");
		} else {
			System.out.println("They are not equal");
		}
	}
}
