package stringdemo;

public class StringCharAt_Int_Index {

	public static void main(String[] args) {
		String str = "Krishz Kara";
		// This will return the first char of the string
		char ch1 = str.charAt(0);

		// This will return the 3rd char of the string
		char ch2 = str.charAt(2);

		// This will return the 9th char of the string
		char ch3 = str.charAt(8);

		System.out.println("Character at 0 index is: " + ch1);
		System.out.println("Character at 2nd index is: " + ch2);
		System.out.println("Character at 8th index is: " + ch3);

	}

}
