package stringdemo;

import java.io.*;

public class ByteGetBytes_StringCharsetName {

	public static void main(String[] args) {

		String Str1 = new String("Welcome to Kara's Java world");
		try {
			String Str2 = new String(Str1.getBytes("UTF-8"));
			System.out.println("Returned Value " + Str2);
			Str2 = new String(Str1.getBytes("ISO-8859-1"));
			System.out.println("Returned Value " + Str2);
		} catch (UnsupportedEncodingException e) {
			System.out.println("Unsupported character set");
		}
	}

}
