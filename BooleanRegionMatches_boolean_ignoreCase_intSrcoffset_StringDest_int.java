package stringdemo;

public class BooleanRegionMatches_boolean_ignoreCase_intSrcoffset_StringDest_int {

	public static void main(String[] args) {

		String Str1 = new String("Welcome to Kara's Java World");
		String Str2 = new String("World");

		System.out.print("Return Value : ");
		System.out.println(Str1.regionMatches(true, 11, Str2, 0, 9));

	}

}
