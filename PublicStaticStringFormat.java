package stringdemo;

import java.util.Locale;

public class PublicStaticStringFormat {

	public static void main(String[] args) {

		double piVal = Math.PI;

		/*
		 * returns a formatted string using the specified format string, and arguments
		 */
		System.out.format("%f\n", piVal);

		/*
		 * returns a formatted string using the specified locale, format string and
		 * arguments
		 */
		System.out.format(Locale.US, "%10.2f", piVal);

	}

}
