package stringdemo;

public class Int_hashCode {

	public static void main(String[] args) {

		Integer i = new Integer("20");

		/*
		 * returns a hash code value for this object, equal to the primitive int value
		 * represented by this Integer object
		 */
		int retval = i.hashCode();
		System.out.println("Value = " + retval);

	}

}
