package stringdemo;

import java.lang.*;

public class IntLastIndexOf_intCh_intFromIndex {

   public static void main(String[] args) {

      String str = "This is Kara's Java World";
   
      /* returns positive value(last occurrence of character t) as character
         is located, which searches character t backward till index 14 */
      System.out.println("last index of letter 't' =  "
         + str.lastIndexOf('t', 14)); 
      
      /* returns -1 as character is not located under the give index,
         which searches character s backward till index 2 */
      System.out.println("last index of letter 's' =  "
         + str.lastIndexOf('s', 2)); 
      
      // returns -1 as character e is not in the string
      System.out.println("last index of letter 'e' =  "
         + str.lastIndexOf('e', 5));
   }
}