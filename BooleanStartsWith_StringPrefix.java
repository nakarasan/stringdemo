package stringdemo;

public class BooleanStartsWith_StringPrefix {

	public static void main(String[] args) {
		String s = "This is just a sample string";

		System.out.println("The String starts with This ?\n" + s.startsWith("This"));

		System.out.println("The String starts with This ?\n" + s.startsWith("Hi"));

	}

}
