package stringdemo;

public class IntLength {

	public static void main(String[] args) {

		String Str1 = new String("Welcome to Kara's Java World");
		String Str2 = new String("Tutorials");

		System.out.print("String Length : ");
		System.out.println(Str1.length());

		System.out.print("String Length : ");
		System.out.println(Str2.length());

	}

}
