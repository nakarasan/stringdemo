package stringdemo;

public class StringReplace_charOldChar_charNewChar {

	public static void main(String[] args) {

		String Str = new String("Welcome to Kara's Java World");

		System.out.print("Return Value :");
		System.out.println(Str.replace('o', 'T'));
		
		System.out.print("Return Value :");
		System.out.println(Str.replace('l', 'D'));
	}

}
